# Accélérations de requêtes de voisinage

Ce TP aborde la géométrie algorithmique, et plus particulièrement les problèmes
de requêtes de voisinages : étant donné un ensemble de points et une requête (un
autre point), déterminer le point de l'ensemble le plus proche de la requête. Le
but est surtout ici de répondre à ce problème lorsque l'ensemble de points ne
varie pas, mais que beaucoup de requêtes de voisinage sont réalisées. Dans ce
cas il est alors possible de commencer par organiser l'ensemble de points pour
pouvoir par la suite réaliser les requêtes plus efficacement.

## Base de code

Une base de code en Python vous est fournie pour ce TP. Elle fournit un ensemble
de tests suivant la progression du TP pour tester au fur et à mesure la mise en
place des fonctionnalités demandées. Par défaut aucun test n'est actif, à vous
de les activer lorsque vous avez mis en place la fonctionnalité correspondante. 

La base Python se repose sur la bibliothèque `numpy` pour l'algèbre (points,
vecteurs, matrices, opérations de base), et la bibliothèque `matplotlib` pour
visualiser les résultats. Deux modules sont également fournis : le fichier
`sampling.py` contient des utilitaires pour générer aléatoirement des points,
directions et couleurs. Le fichier `plotting.py` contient des fonctions pour
faciliter la visualisation des objets géométriques que nous manipulerons dans ce
TP.

Libre à vous de créer de nouveaux modules ou d'ajouter vos développement à
l'endroit qui vous semble le plus approprié. Le but ici est surtout de se
concentrer sur les aspects algorithmiques. Pour que certains tests fonctionnent,
il sera néanmoins nécessaire de respecter certaines contraintes sur les noms des
fonctions et leur prototype, ou de modifier la base de tests pour l'adapter à
votre code.

## Plus proche voisin naif

Pour démarrer, mettez en place une fonction prenant en paramètre un tableau de
points et une requête et retournant l'indice du point le plus proche dans le
tableau de points.

#### Contraintes

La fonction à réaliser doit s'appeler `naive_nearest` et suivre le prototype
suivant :

```py
naive_nearest(points, query) -> int
```

Ici `points` est un tableau de points, `query` est un point (la requête), et
l'entier `nn` retourné est tel que `points[nn]` est le point le plus proche de
`query` dans `points`.

#### Test à activer 

Le test à activer s'appelle `nn_test`. Il utilise par défaut un ensemble de 100
points, et réalise 5000 requêtes de voisinages. Si votre fonction est correcte,
vous devriez obtenir joli feu d'artifice :

<center>
<img id="nn-test" src="img/nn.svg", width=500px"></img>
</center>

Le test dessine pour chaque requête un segment la reliant à son plus proche voisin
parmi les 100 points, et colore les requêtes et les segments de la couleur de
leur point le plus proche.

## Boîtes englobantes alignées sur les axes

Pour accélérer cet algorithme naïf, nous allons utiliser la stratégie des
volumes englobant. L'idée est de regrouper les points par paquets, et de
calculer pour chaque paquet un volume englobant. L'englobant choisi doit être
tel qu'il est facile de calculer la distance entre une requête et un englobant.
Ainsi, si nous avons pour une requête déjà trouvé un point assez proche (le
candidat), nous pouvons comparer la distance entre la requête et l'englobant à
la distance entre la requête et le candidat. Si l'englobant est plus éloigné, il
est alors possible d'ignorer tous les points qu'il contient sans avoir à les
tester un par un.

Pour ce TP nous utiliserons des *boîtes englobantes alignées sur les axes*. Il
s'agit du rectangle minimal contenant tous les points, et dont les côtés sont
verticaux et horizontaux.

### Construction

Étant donné un ensemble de points, quelles sont les coordonnées des sommets de
leur boîte englobante alignée sur les axes, en fonction des coordonnées des
points qu'elle doit contenir ? Vous pouvez observer [l'image que le test doit
fournir](#box-build-test) pour visualiser le rectangle recherché.

La boîte étant rectangulaire avec des côtés horizontaux et verticaux, elle est
complètement définie en ne stockant que son sommet en bas à gauche et son sommet
en haut à droite. Créez donc une classe `Box` qui contiendra ces deux points, et
qui sera munie d'une méthode `push` pour ajouter de nouveaux points à la boîte.
La boîte ne stocke pas les points ajoutés, mais uniquement les deux coins de la
boîte englobante.

#### Contraintes

La classe réalisée doit s'appeler `Box`, et les deux points stockés doivent
s'appeler `self.min` et `self.max`. Elle doit être munie d'un constructeur qui initialise
ces deux points à `None` et d'une méthode `push` avec le prototype :

```py
push(self, point) -> None
```

#### Test à activer

Le test à activer s'appelle `box_build_test`. Il génère aléatoirement un
ensemble de 100 points, puis construit leur boîte englobante en en créant une
vide, et en les poussant un par un. Il affiche ensuite les points et la boîte
pour s'assurer qu'elle les contient tous et n'est pas trop grande. Si tout se
passe bien, vous devriez obtenir l'image suivante :

<center>
<img id="box-build-test" src="img/box_build.svg", width=500px"></img>
</center>

### Point de la boîte le plus proche d'une requête

Pour pouvoir exploiter nos boîtes englobantes dans les requêtes de voisinage, il
est nécessaire de pouvoir calculer la distance entre une requête et une boîte.
Selon la position de la requête, son point le plus proche varie. Si la requête
est dans la boîte, sa distance à la boîte est nulle, et elle est son propre
point le plus proche dans la boîte. Si elle est en dehors de la boîte, faites
vous un dessin au brouillon et des exemples pour déterminer visuellement les
différents cas possibles. Vous pouvez ensuite en déduire l'algorithme pour
calculer étant donné une requête les coordonnées de son point le plus proche
dans la boîte. Vous pouvez également vous inspirer de [l'image que le test doit
fournir](#box-nearest-text).

<details style="margin: 1em">
<summary style="margin-bottom: 1em">Cliquez pour révéler un indice</summary>

Pour chaque coordonnée de la requête (x et y) le point le plus proche peut avoir
trois coordonnées possible : la coordonnée de la requête, celle du point `min`
de la boîte, ou celle du point `max` de la boîte. Déterminez le test à réaliser
pour identifier les cas.

</details>

Ajoutez une méthode `nearest` à votre classe `Box` pour obtenir le point le plus
proche d'une requête.

#### Contraintes

Votre méthode doit avoir le prototype:

```py
nearest(self, point) -> point
```

#### Test à activer

Le test s'appelle `box_nearest_test` et génère une boîte aléatoire pas trop
petite, puis calcule le point le plus proche d'une centaine de requêtes
aléatoires. Pour chaque requête, il dessine un segment reliant la requête à son
point le plus proche, ainsi que le point le plus proche avec une couleur
correspondante. Si tout se passe bien, vous devriez obtenir :

<center>
<img id="box-nearest-test" src="img/box_nearest.svg", width=500px"></img>
</center>

## Hiérarchie de boîtes englobantes

Muni de boîtes englobantes, il faut maintenant se préoccuper de la méthode pour
regrouper les points dans ces boîtes.

### Arbre binaire

Nous allons utiliser une technique similaire à celle des arbres binaires de
recherche : nous allons créer un arbre binaire où chaque nœud est responsable
d'une partie des points de l'ensemble initial, et stocke leur boîte englobante.
Au niveau de chaque nœud, les points référencés par le parent sont répartis
entre les deux enfants.

Implémentez une classe `Tree` pour encoder les nœuds d'un tel arbre. Elle devra
donc stocker une boîte englobante, ainsi que les références vers les deux
enfants. Pour référencer une partie des points, chaque nœud stockera l'indice du
premier point qu'il référence dans le tableau global des points, et l'indice du
premier point qu'il ne référence plus.

#### Contraintes

Votre classe doit s'appeler `Tree`. Son constructeur doit avoir le prototype 

```py
__init__(self, points, debut, fin)
```

où `points` est le tableau de points, `debut` est l'indice du premier point à
référencer, et fin l'indice du premier point à ne plus référencer. Ces deux
indices doivent être stockés en interne dans deux attributs `self.begin` et
`self.end`. La boîte englobante doit s'appeler `self.box` et le constructeur
doit l'initialiser pour contenir les points référencés. Enfin les deux enfants
doivent être référencés dans un tableau `self.child` à deux éléments, et chaque
enfant doit être initialisé à `None`. Si `t` est une variable de classe `Tree`,
ses enfants s'obtiennent donc en demandant `t.child[0]` et `t.child[1]`, et il
est possible de déterminer si le nœud a des enfants en vérifiant si l'un ou
l'autre des enfants est `None`.

### Découpe d'un nœud

Il existe de multiples stratégies pour déterminer à chaque nœud comment répartir
ses points entre ses enfants. Une stratégie classique consiste à essayer de
réduire rapidement la taille des boîtes en déterminant dans quel sens elle est
la plus grande (largeur ou hauteur), et en la coupant en deux dans cette
direction. Mettons que la boîte soit plus large que haute, nous déterminons
alors la coordonnée `x_mid` du centre de la boîte, et répartissons les points
selon que leur coordonnée `x` est plus grande ou plus petite que `x_mid`.

Étant donné qu'un nœud référence une portion contiguë du tableau de points en ne
stockant que deux indices, il est nécessaire de réordonner les points localement
pour que les points ayant `x ≤ x_mid` soient tous placés avant les points ayant
`x > x_mid`. Attention à ne réordonner que les points référencés par le parent.
Pour réordonner les points, l'algorithme est assez classique : il s'agit de la
technique du pivot utilisée par exemple dans l'algorithme du tri rapide
(quicksort) ou pour calculer rapidement une médiane (quickselect).

<details style="margin: 1em">
<summary style="margin-bottom: 1em">Cliquez pour révéler de l'aide</summary>

Pour pivoter une portion d'un tableau, l'algorithme va traiter ses éléments un
par un et réaliser des échanges dans le tableau. À chaque itération de
l'algorithme, la portion du tableau à réordonner est découpée en trois zones :

* les éléments plus petits ou égaux au pivot ;
* les éléments plus grands que le pivot ;
* les éléments restant à traiter.

Une itération consiste à traiter un point en le déplaçant dans sa zone. Une
méthode simple consiste à ordonner le tableau comme suit :

```
-------------------+-----------+-----------+-----------+-------------------
ne pas traiter ... | x ≤ x_mid | à traiter | x > x_mid | ... ne pas traiter
-------------------+-----------+-----------+-----------+-------------------
```
Ici les points du tableau avant le `begin` du parent, et ceux à partir du `end`
du parent ne sont pas à traiter. Pour identifier les zones, il suffit de stocker
deux indices :

* `inf` : l'indice du premier point de la zone `à traiter` ;
* `sup` : l'indice du premier point de la zone `x > x_mid`.

Une stratégie simple consiste à traiter à chaque itération le point à l'indice
`inf`. Si ce point est tel que `x ≤ x_mid`, le point est laissé là, et `inf` est
incrémenté. Sinon, il faut l'échanger avec le dernier point de la zone à
traiter. Il suffit donc de commencer par décrémenter `sup` puis d'échanger les
points aux indices `inf` et `sup`. Lorsque `inf` et `sup` sont égaux, tous les
points sont traités et ordonnés.

</details>

Implémentez une méthode de la classe `Tree` pour réaliser cette découpe. Cette
méthode déterminera l'axe le long duquel découper la boîte, la coordonnée pivot
pour répartir les points, pivotera la portion du tableau de points référencée
par le nœud, puis créera deux enfants référant les deux portions de tableau.

#### Contraintes

Votre méthode doit avoir le prototype

```py
split_sort(self, points) -> None
```

#### Test à activer

Pour tester votre découpe, activez le test `tree_split_test`. Ce test construit
un nœud référençant un ensemble d'une centaine de points, puis appelle la
méthode `split_sort` de ce nœud. Il affiche ensuite en vert et en rouge les deux
sous-ensembles de points créés, ainsi que les boîtes englobantes du parent et
des enfants créés. Vous devriez donc obtenir une image comme ci-dessous.

<center>
<img id="tree-split-test" src="img/tree_split.svg", width=500px"></img>
</center>

### Création d'un arbre complet

Une fois la découpe fonctionnelle, il ne reste plus qu'à découper récursivement
les nœuds pour former un arbre. La racine est initialisée pour référencer
l'ensemble des points, puis tant que les nœuds référencent plus d'un certain
nombre de points, ils sont découpés.

Ajoutez une méthode à votre classe `Tree` pour réaliser cette découpe récursive.

#### Contraintes

Votre méthode soit avoir le prototype

```py
split_rec(self, points, max_points)
```

où `max_points` est le nombre de points à partir duquel les nœuds ne sont plus
subdivisés.

#### Test à activer

Activez le test `tree_create_test` pour visualiser l'arbre ainsi créé. Ce test
initialise un arbre sur une centaine de points, appelle la méthode `split_rec`
avec par défaut un seuil à 5 points, et enfin affiche les boîtes englobantes de
tous les nœuds de l'arbre créé, ainsi que les points référencés par les feuilles
de l'arbre d'une couleur assortie à celle de leur feuille. Vous devriez ainsi
obtenir une image comme ci-dessous.

<center>
<img id="tree-create-test" src="img/tree_create.svg", width=500px"></img>
</center>

### Plus proche voisin dans un arbre

Vous pouvez désormais exploiter votre arbre pour accélérer vos requêtes de
voisinage. L'idée est de commencer à la racine de l'arbre, et de maintenir un
point candidat pour être le plus proche de la requête, ainsi que sa distance à
la requête. À chaque nœud, si la distance entre la boîte de ses enfants et la
requête est plus petite que la distance du point candidat, l'enfant est visité,
ce qui mettra à jour si besoin le point candidat et sa distance. Pour plus
d'efficacité, il faut visiter d'abord l'enfant le plus proche de la requête.

Créez une fonction récursive pour réaliser cette requête de voisinage améliorée.

#### Contraintes

Votre fonction doit avoir le prototype

```py
tree_nearest(points, tree, query) -> int
```

Cette fonction doit renvoyer l'indice du point le plus proche de `query` dans le
tableau `points`. 

#### Test à activer

Activez ensuite le test `tree_nn_test` pour tester votre fonction. Il faut
également que le test `nn_test` soit actif. Ce test se rajoute à celui de votre
algorithme naïf et il vérifie que les résultats des deux algorithmes sont
identiques. [L'image produite ne devrait pas changer](#nn-test). Le test mesure également
les temps d'exécution pour vérifier que cette méthode est plus rapide que la
naïve.

## Lancer de rayons de Voronoi

Étant donné un ensemble de points (les sites), le diagramme de Voronoi associe à
chaque site une *cellule* : l'ensemble des points de l'espace pour lesquels il
est le site le plus proche. Ces cellules sont les polygones que vous pouvez voir
sur l'animation ci-dessous. Le but de cette partie est de construire les
cellules de Voronoi en utilisant une approche par lancer de rayons. Nous allons
ainsi chercher à déterminer étant donné une source et une direction à quel
moment un point partant de la source et se déplaçant dans la direction donnée
sortira de la cellule de Voronoi d'où il est parti.

<center>
<img id="voronoi-exemple" src="img/voronoi.svg", width=500px"></img>
</center>

### Intersection rayon médiatrice

Les cellules de Voronoi sont des polygones, et chaque arête de ces polygones est
un morceau de médiatrice entre deux sites. Commencez donc par écrire une
fonction permettant de calculer le point d'intersection entre un rayon défini
par une source et une direction et une médiatrice définie par deux sites. Ce
point doit répondre à deux contraintes :

* être sur le rayon ;
* être sur la médiatrice.

Exprimez ces deux contraintes par des équations linéaires en fonction des
positions des sites, de la source et de la direction. Résolvez ce système et
codez la fonction calculant ce résultat.

<details style="margin: 1em">
<summary style="margin-bottom: 1em">Cliquez pour révéler de l'aide</summary>

Posons :
* $`s`$ : la source ;
* $`d`$ : la direction ;

Un point $`p`$ est sur le rayon s'il peut s'écrire $`p = s + t.d`$ avec t un
réel positif. Trouver le point 

<details style="margin: 1em">
<summary style="margin-bottom: 1em">Cliquez pour révéler plus d'aide</summary>

Soient $`b_0`$ et $`b_1`$ les deux sites définissant la médiatrice. La médiatrice est perpendiculaire au segment $`[b_0,b_1]`$ et passe par le milieu de ce segment, soit le point $`m = \frac{b_0 + b_1}{2}`$. Un point $`p`$ est sur la médiatrice si le segment $`[m,p]`$ est perpendiculaire au segment $`[b_0,b_1]`$. Cette relation peut s'exprimer à l'aide du produit scalaire comme $`(m - p).(b_1-b_0) = 0`$.

<details style="margin: 1em">
<summary style="margin-bottom: 1em">Cliquez pour révéler encore plus d'aide</summary>

En combinant les deux relations, nous cherchons $`t`$ tel que 

```math
\begin{aligned}
  0 &= \left(\frac{b_0 + b_1}{2} - s - t.d\right).(b_1 - b_0) \\\\
  \Leftrightarrow t &= \frac{(b_0 + b_1 - 2s).(b_1 - b_0)}{2d.(b_1 - b_0)}
\end{aligned}
```

Une fois $`t`$ calculé, il ne reste qu'à calculer $`s + t.d`$ pour obtenir le
point d'intersection.

</details>
</details>
</details>

#### Contraintes

Votre fonction doit avoir le prototype

```py
bisector_ray_intersect(b0, b1, source, direction) -> point
```

#### Test à activer

Pour tester votre fonction, activez le test `bisector_ray_intersect_test`. Ce
test génère deux sites assez éloignés, et un ensemble de points aléatoires. Il
trie ensuite les points selon leur site le plus proche, et constitue des rayons
assurés de créer une intersection avec la médiatrice des sites. Il calcule
enfin les intersection de tous les rayons avec la médiatrice des deux sites et
trace le résultat. Si tout se passe bien vous devriez obtenir un image comme
ci-dessous.

<center>
<img id="bisector_ray_intersect" src="img/bisector_ray_intersect.svg", width=500px"></img>
</center>

Les points noirs sont les intersections, et les rayons s'éclaircissent en
traversant la médiatrice.

### Approche naive

Si $`b_0`$ est le site le plus proche de la source $`s`$ du rayon, il est
possible de montrer que chercher l'intersection entre le rayon et le bord de la
cellule de $`b_0`$ revient à chercher un autre site $`b_1`$ tel que le cercle
centré sur le rayon et passant par $`b_0`$ et $`b_1`$ ne contienne aucun autre
site. 

Une approche naïve pour trouver $`b_1`$ consiste donc à parcourir tous les sites
linéairement en maintenant un site *candidat*. Pour chaque site, s'il se trouve
*dans* le cercle centré sur le rayon et passant par $`b_0`$ et le candidat,
alors ce site devient le nouveau candidat, et le centre du cercle peut être mis
à jour par intersection entre le rayon et la médiatrice entre $`b_0`$ et le
candidat. Pour tester si un site est dans le cercle, il suffit de tester si sa
distance au centre du cercle est plus petite que le rayon du cercle (distance
entre son centre et le candidat). Ce processus est illustré sur l'animation
ci-dessous.

<center>
<img id="animated-raytrace" src="img/raytrace_steps.gif", width=500px"></img>
</center>

Dans cette animation, les sites deviennent noir lorsqu'ils ont été traités. On
peut voir le cercle centré sur le rayon et passant par le candidat et $`b_0`$ se
réduire petit à petit jusqu'à s'arrêter sur le $`b_1`$ souhaité.

**Attention :** il faut faire attention à l'initialisation du candidat pour ne
pas trouver d'intersection en dehors du rayon (avant le point de départ). En
pratique, on peut montrer qu'il est inutile de considérer les sites $`b`$ tels
que $`(b_0 - b).d > 0`$. D'autre part, il faut également ignorer le site
$`b_0`$, mais le supérieur strict de l'inégalité précédente est normalement
suffisant.

#### Contraintes

Votre fonction doit avoir le prototype 

```py
naive_raytrace(sites, b_0, source, direction) -> int
```

Ici $`b_0`$ est l'indice du site le plus proche de la source. La fonction
renvoie l'indice du site $`b_1`$ dans le tableau de sites tel que la médiatrice
de $`[b_0, b_1]`$ est correspond au côté de la cellule de Voronoi que le rayon
intersecte en sortant de la cellule.

#### Test à activer

Pour tester cette fonction, activez le test `raytrace_test`. Il utilise par
défaut un ensemble de 100 sites et génère 5000 sources et directions. Il trace
ensuite ces rayons en les stoppant au bord de leur cellule de Voronoi de départ,
et indique en noir les points d'intersection trouvés. Si tout se passe bien vous
devriez obtenir une image ressemblant à celle ci-dessous.

<center>
<img id="raytrace-test" src="img/raytrace.svg", width=500px"></img>
</center>

### Utilisation de la hiérarchie de boîtes englobantes

Il est possible d'accélérer cette recherche en utilisant votre hiérarchie de
volumes englobants. En effet, si la distance entre le centre du cercle du site
candidat et la boîte englobante est plus grande que le rayon du cercle, alors il
n'est pas nécessaire d'examiner le contenu de la boîte. Cette approche revient
donc à parcourir l'arbre d'une façon très similaire à une requête de voisinage
classique, mais à chaque fois que le site candidat est modifié, le centre du
cercle et son rayon doivent être mis à jour.

#### Contraintes

Votre fonction doit avoir le prototype

```py
tree_raytrace(sites, tree, b0, source, direction)
```

#### Test à activer

Pour tester votre fonction, activez le test `tree_raytrace_test`. Le test
`raytrace_test` doit également être activé. Ce test vérifie que le résultat de
votre nouvelle fonction est identique à celui de la fonction naïve, et vous
devriez normalement voir apparaître un meilleur temps de calcul dans la console.

## Cellules de Voronoi

Pour conclure, nous allons maintenant utiliser notre lancer de rayons pour
calculer les cellules de Voronoi.

### Cas des cellules simples

Le principe pour calculer la cellule de Voronoi d'un site consiste à partir du
site et tirer un rayon dans une direction quelconque pour trouver le bord de sa
cellule. Ensuite, nous tirerons des rayons le long des arêtes pour trouver les
sommets du polygone un par un. Dans cette partie, nous ne traitons que les
cellules simples, c'est à dire celles qui sont bornées (certaines cellules sont
infinies).

Le premier rayon est simple à réaliser si vous avez les fonctions précédentes.
Pour les rayons suivants, par contre, étant donné qu'ils se confondent avec les
arêtes, ils peuvent engendrer des cas dégénérés. Il est donc nécessaire de
modifier légèrement votre fonction de lancer de rayons pour gérer ce cas.
Ajoutez-lui un paramètre optionnel qui permet d'ignorer un site. Ainsi, lorsque
vous lancez un rayon le long de l'arête qui correspond à la médiatrice des
sites $`b_0`$ et $`b_1`$, vous pouvez demander à ignorer le site $`b_1`$ pour ne
pas le retrouver et éviter de devoir calculer une intersection dégénérée entre
le rayon et la médiatrice qui le contient.

D'autre part, il est nécessaire de calculer après chaque sommet trouvé une
nouvelle direction. Il faut ici faire attention à ne pas faire demi-tour.

<details style="margin: 1em">
<summary style="margin-bottom: 1em">Cliquez pour révéler de l'aide</summary>

Si $`s`$ est le dernier sommet que vous avez découvert, et si $`b_1`$ est le
site dont la médiatrice avec $`b_0`$ définit la prochaine arête à suivre, vous
pouvez utiliser le fait que le point $`m = \frac{b_0 + b_1}{2}`$ est sur l'arête
pour calculer une direction le long de l'arête comme $`s - m`$. Attention
cependant cette direction n'est pas assurée de toujours tourner dans le même
sens autour de la cellule de Voronoi.

<details style="margin: 1em">
<summary style="margin-bottom: 1em">Cliquez pour révéler plus d'aide</summary>

Pour vous assurer de toujours tourner dans le même sens, si $`s`$ est le dernier
sommet trouvé, et si $`d`$ est la direction de la prochaine arête, vous pouvez
vous assurer que $`d`$ tourne dans le sens souhaité en regardant le signe du
déterminant de la matrice 2x2 ayant pour colonnes $`s - b_0`$ et $`d`$. Le signe
de ce déterminant est le même que celui du sinus de l'angle entre les vecteurs 
$`s - b_0`$ et $`d`$. Si ce sinus est positif alors $`d`$ permet de tourner dans
le sens positif (contraire des aiguilles d'une montre). 

</details>
</details>

#### Contraintes

Votre fonction doit avoir le prototype

```py
voronoi_cell(sites, tree, b0) -> list[points]
```

Cette fonction renvoie la liste des sommets de la cellule de Voronoi du site `b0`.

#### Test à activer

Activez le test `voronoi_cell_test` pour vérifier votre fonction. Ce test
calcule et affiche les cellules de Voronoi de sites centraux dans le diagramme,
ces sites ayant très probablement des cellules de Voronoi bornées. Si tout va
bien vous devriez obtenir une image similaire à la suivante.

<center>
<img id="voronoi-cell-test" src="img/voronoi_simple.svg", width=500px"></img>
</center>

### Gestion des cellules infinies ou trop grandes

Pour conclure, faites désormais en sorte de gérer les cellules infinies.
Pour détecter qu'une cellule est infinie, normalement lorsque vous lancez votre
rayon pour trouver le bord de la cellule, vous ne trouvez aucun candidat valide.
Ainsi, si vous initialisez votre résultat à `None`, votre fonction retourne `None`.

Le premier problème est de trouver le bord de la cellule de Voronoi. Vous pouvez
néanmoins raisonnablement estimer qu'en lançant un rayon dans la direction
opposée à celle qui a trouvé un rayon infini, vous trouverez le bord. En effet
pour qu'une cellule soit infinie dans deux direction opposées, il est nécessaire
que les sites soient tous alignés sur une droite, et que la direction soit
perpendiculaire à cette droite. [Des techniques permettent de traiter ce genre de
situation de façon robuste](https://arxiv.org/abs/math/9410209), mais ce n'est
pas ici notre but.

Le second problème est de trouver tous les sommets. En effet, en ne tournant que
dans un sens, vous ne pourrez pas aller plus loin que le rayon infini. Il existe
ici plusieurs solutions. Vous pouvez décider de faire demi tour lorsque vous
trouvez un rayon infini, et ainsi d'énumérer tous les sommets en tournant dans
l'autre sens jusqu'à trouver un autre rayon infini. Vous pouvez également
calculer la cellule restreinte au carré unité, et donc dans ce cas détecter
également les intersections entre les rayons et le carré unité. Avec ces
intersections, vous pouvez alors calculer l'intersection entre la cellule et le
carré unité, qui ne peut pas être infinie. C'est la solution employée pour
obtenir l'image finale ci-dessous.

<center>
<img id="voronoi-test" src="img/voronoi.svg", width=500px"></img>
</center>
