import random
import matplotlib
import numpy as np

# Utilitaires pour générer aléatoirement des trucs

# Direction aléatoire sur la sphère unité
def rand_direction(size = 1, dim = 2):
    # distribution normale sur tous les axes
    pts = np.random.normal(0,1,(size, dim))
    # points uniformes sur la sphere
    pts = pts / np.linalg.norm(pts, axis=1)[:, np.newaxis]
    if size == 1:
        return pts[0]
    else:
        return pts
    
# Points aléatoires dans la boule unité
def rand_in_disk(size = 1, center = np.array([0,0]), radius = 1):
    dim = len(center)
    pts = rand_direction(size, dim)
    # points uniforme dans la boule
    pts = pts * (np.random.uniform(0,1,size)**(1/dim))[:, np.newaxis]
    # recentrage et redimensionnement
    pts = center + radius * pts
    return pts
    
# Points aléatoires dans le carré unité
def rand_in_square(size = 1, dim = 2):
    pts = np.random.uniform(0,1,(size, dim))
    if size == 1:
        return pts[0]
    else:
        return pts

# Couleurs aléatoires
def rand_color(size = 1) :
    colors = rand_in_square(size, 3)
    colors = colors * np.array([1, 0.2, 0.3]) + np.array([0, 0.4, 0.7])
    import matplotlib.colors as mc
    colors = mc.hsv_to_rgb(colors)
    return colors

def pastel(color, amount=0.5):
    import matplotlib.colors as mc
    try:
        c = mc.cnames[color]
    except:
        c = color
    c = mc.rgb_to_hsv(mc.to_rgb(c))
    return mc.hsv_to_rgb([c[0], amount*c[1], amount + (1-amount)*c[2]])
