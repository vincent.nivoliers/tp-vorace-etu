import sampling
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

#default plot configuration
def new_plot():
    fig, ax = plt.subplots(figsize = (8,8))
    plt.axis('off')
    ax.set_xlim(-0.01,1.01)
    ax.set_ylim(-0.01,1.01)
    return (fig, ax)

#plot point sets
def plot_point(point, **args):
    return plt.scatter(point[0], point[1], **args)

def plot_points(points, **args):
    return plt.scatter(points[:,0], points[:,1], **args)

#plot polygonal lines
def plot_line(points, **args):
    defaults = {"color" : "black", **args}
    return plt.plot(points[:,0], points[:,1], **defaults)

def plot_lines(lines, **args):
    defaults = {"color" : "black", **args}
    l = matplotlib.collections.LineCollection(lines, **defaults)
    plt.gca().add_collection(l)
    return l

#plot polygons

def plot_polygons(lines, **args):
    defaults = {"edgecolor" : "black", **args}
    l = matplotlib.collections.PolyCollection(lines, **defaults)
    plt.gca().add_collection(l)
    return l

# Tracé de boîtes
def plot_box(box, color = None, **args):
    if color is None:
        color = sampling.rand_color()
    corners = np.empty((4,2))
    corners[0] = box.min
    corners[1] = [box.max[0], box.min[1]]
    corners[2] = box.max
    corners[3] = [box.min[0], box.max[1]]
    plt.fill(corners[:,0], corners[:,1], facecolor=sampling.pastel(color, 0.2), zorder=args.get("zorder"))
    plt.fill(corners[:,0], corners[:,1], facecolor='none', edgecolor=color, **args)
