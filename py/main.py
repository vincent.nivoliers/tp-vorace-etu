from sampling import *
from plotting import *

import numpy as np
from numpy.linalg import norm
import time

nn_test = False
box_build_test = False
box_nearest_test = False
tree_split_test = False
tree_create_test = False
tree_nn_test = False
bisector_ray_intersect_test = False
raytrace_test = False
tree_raytrace_test = False
voronoi_cell_test = False
full_diagram_test = False

use_tree = tree_create_test or tree_nn_test or raytrace_test or voronoi_cell_test

# You can add code here

if __name__ == "__main__":

    random.seed("katherine johnson")
    np.random.seed(random.getrandbits(32))

    point_size = 100
    query_size = 5000

    pts = rand_in_square(point_size)
    queries = rand_in_square(query_size)
    colors = rand_color(point_size)

    tree = None

    if use_tree:

        tree = Tree(pts, 0, point_size)
        tree.split_rec(pts, 5)

    if nn_test:

        #compute nearest neighbors naively
        naive_start = time.perf_counter()
        nn = [ naive_nearest(pts, q) for q in queries ]
        naive_end = time.perf_counter()
        print(f"naive nearest neighbors performed in {naive_end - naive_start} seconds")

        if tree_nn_test:
            #compute nearest neighbors using the tree
            naive_nn = nn
            tree_start = time.perf_counter()
            nn = [ tree_nearest(pts, tree, q) for q in queries ]
            tree_end = time.perf_counter()
            assert nn == naive_nn
            print(f"tree nearest neighbors performed in {tree_end - tree_start} seconds")

        fig,ax = new_plot()

        #plot each query linked to its nearest neighbor with matching colors
        segments = list(zip(queries, pts[nn]))
        query_colors = colors[nn]
        plot_lines(segments, colors=query_colors, zorder = 1)
        plot_points(queries, c=query_colors, s=4, zorder = 2)
        plot_points(pts, c=colors, edgecolor="black", zorder=3)

        #plt.show()
        print("saved nearest neighbor test as nn.svg")
        fig.savefig("nn.svg")

    if box_build_test:

        box_pts = pts * 0.2 +  np.array([0.3, 0.6])
        box = Box()
        for pt in box_pts:
            box.push(pt)
        
        fig,ax = new_plot()

        plot_box(box) ;
        plot_points(box_pts, c=colors, edgecolor="black", zorder=3)

        #plt.show()
        print("saved box construction test as box_build.svg")
        fig.savefig("box_build.svg")

    if box_nearest_test:

        box = Box()
        box.push(rand_in_square() * 0.3 + 0.1)
        box.push(rand_in_square() * 0.3 + 0.6)

        nn_pts = np.array([box.nearest(p) for p in pts])

        fig,ax = new_plot()

        segments = [[pts[i], nn_pts[i]] for i in range(point_size)]
        plot_lines(segments, zorder = 1)
        plot_box(box, zorder = 1)
        plot_points(pts, c=colors, edgecolor = "black", zorder = 2)
        plot_points(nn_pts, c=colors, edgecolor = "black", zorder = 2)

        #plt.show()
        print("saved box nearest test as box_nearest.svg")
        fig.savefig("box_nearest.svg")

    if tree_split_test:

        node = Tree(pts, 0, point_size)
        node.split_sort(pts)

        fig,ax = new_plot()

        plot_box(node.box, zorder = 0)
        plot_box(node.child[0].box, zorder = 1)
        plot_box(node.child[1].box, zorder = 1)

        plot_points(pts, c=colors, edgecolor="black", zorder = 2)
        plot_points(pts[node.child[0].begin:node.child[0].end,:], c="green", edgecolor="black", zorder = 2)
        plot_points(pts[node.child[1].begin:node.child[1].end,:], c="red", edgecolor="black", zorder = 2)

        #plt.show()
        print("saved tree split test as tree_split.svg")
        fig.savefig("tree_split.svg")

    if tree_create_test:

        fig,ax = new_plot()

        stack = [tree]
        while len(stack) > 0:
            color = rand_color()
            node = stack.pop()
            plot_box(node.box, color=color, zorder=0)
            if node.child[0] is not None:
                stack.append(node.child[0])
                stack.append(node.child[1])
            else:
                plot_points(pts[node.begin:node.end,:], color=color, edgecolor="black", zorder=1)

        #plt.show()
        print("saved tree construction test as tree_create.svg")
        fig.savefig("tree_create.svg")

    if bisector_ray_intersect_test:

        #generate two sites with corresponding colors
        s0 = rand_in_square()
        s0[0] *= 0.5
        c0 = rand_color()

        s1 = rand_in_square()
        s1[0] = 0.5*(1 + s1[0])
        c1 = rand_color()

        #generate points and determine their nearest site
        source_size = point_size
        sources = []
        pos = 0

        for i in range(source_size):
            sources.append(rand_in_square())
            if norm(sources[-1] - s0) < norm(sources[-1] - s1):
                sources[pos], sources[-1] = sources[-1], sources[pos]
                pos += 1

        sources = np.array(sources)

        #generate rays
        destinations = [
                *random.choices(sources[pos:], k=pos), 
                *random.choices(sources[:pos], k=source_size - pos)
                ]

        #intersections
        hits = [bisector_ray_intersect(s0, s1, p0, p1) for p0, p1 in zip(sources, destinations - sources)]
        hits = np.array(hits)

        #plot
        fig,ax = new_plot()

        lines_before = list(zip(sources, hits))
        lines_after = list(zip(hits, destinations))

        plot_lines(lines_before[:pos], color=c0, zorder = 1)
        plot_lines(lines_after[:pos], color=c0, zorder = 0, alpha=0.2)
        plot_lines(lines_before[pos:], color=c1, zorder = 1)
        plot_lines(lines_after[pos:], color=c1, zorder = 0, alpha = 0.2)

        plot_points(sources[:pos], color=c0, s=4, zorder=2)
        plot_points(sources[pos:], color=c1, s=4, zorder=2)
        plot_points(hits, color="black", s=4, zorder=2)
        plot_points(np.array([s0, s1]), c = np.array([c0, c1]), edgecolor="black", zorder=2)

        #plt.show()
        print("saved bisector ray intersction test as bisector_ray_intersect.svg")
        fig.savefig("bisector_ray_intersect.svg")

    if raytrace_test:

        #generate ray directions
        directions = rand_direction(query_size)

        #compute nearest neighbors
        nn = [tree_nearest(pts, tree, query) for query in queries]

        #compute raytrace intersections
        naive_start = time.perf_counter() ;
        bisectors = [naive_raytrace(pts, nn, query, direction) for nn,query,direction in zip(nn, queries, directions)]
        naive_end = time.perf_counter() ;
        print(f"naive raytrace queries performed in {naive_end - naive_start} seconds")

        if tree_raytrace_test:
            
            #backup naive bisectors
            naive_bisectors = bisectors
            tree_start = time.perf_counter() ;
            bisectors = [tree_raytrace(pts, tree, nn, query, direction) for nn,query,direction in zip(nn, queries, directions)]
            tree_end = time.perf_counter() ;
            assert bisectors == naive_bisectors
            print(f"tree raytrace queries performed in {tree_end - tree_start} seconds")

        hits = np.array([
                bisector_ray_intersect(pts[n], pts[b], q, d)
                if b is not None else pts[n]
                for (n, b, q, d) in zip(nn, bisectors, queries, directions)
                ])

        #plot the rays and intersections
        fig,ax = new_plot()

        #rays
        plot_lines(list(zip(queries, hits)), color=colors[nn], zorder=0)

        #queries
        plot_points(queries, c=colors[nn], s=4, zorder=1)

        #hits
        plot_points(hits, color="black", s=4, zorder=1)

        #sites
        plot_points(pts, c=colors, edgecolor="black", zorder=2)

        #plt.show()
        print("saved raytrace test as raytrace.svg")
        fig.savefig("raytrace.svg")

    if voronoi_cell_test:

        #restrict to central cells for simple test
        if full_diagram_test:
            subset = range(point_size)
        else:
            subset = [ i for i in range(point_size) if in_unit_square(3 * pts[i] - 1)]

        cells = [voronoi_cell(pts, tree, cell) for cell in subset]

        #plot the cells
        fig,ax = new_plot()

        plot_polygons(cells, facecolors=colors[subset], zorder = 0)
        plot_points(pts, c=colors, edgecolor = "black", zorder = 1)

        #plt.show()
        print("saved voronoi test as voronoi.svg")
        fig.savefig("voronoi.svg")
